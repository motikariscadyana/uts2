from tempfile import template
from django.shortcuts import render
from .models import *
# Create your views here.


def kamar(request):
    tampil = Kamar.objects.all()
    template = 'kamar.html'
    context = {
        'row': tampil,
    }
    return render(request, template, context)
