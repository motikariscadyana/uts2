from tempfile import template
from django.shortcuts import render
from .models import *
# Create your views here.
def karyawan(request):
	tampil = Karyawan.objects.all()
	template = 'karyawan.html'
	context = {
		'petugas' : tampil,
	}
	return render(request, template, context)