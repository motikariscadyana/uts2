from django.contrib import admin
from django.urls import path,include
from django.urls import path

from . import views


urlpatterns = [
    path('admin/', admin.site.urls),

    path('index/', views.index, name='index'),
    
    path('kamar/', include('kamar.urls')),
    path('karyawan/', include('karyawan.urls')),
    path('pengunjung/', include('pengunjung.urls')),

]