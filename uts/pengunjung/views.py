from tempfile import template
from django.shortcuts import render
from .models import *
# Create your views here.
def pengunjung(request):
	tampil = Pengunjung.objects.all()
	template = 'pengunjung.html'
	context = {
		'pe' : tampil,
	}
	return render(request, template, context)