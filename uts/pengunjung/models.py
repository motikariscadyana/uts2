from django.db import models

# Create your models here.


class Pengunjung(models.Model):
    nama = models.CharField(max_length=50)
    alamat = models.CharField(max_length=100)
    jenis_kelamin = models.CharField(max_length=50)
    no_telp = models.CharField(max_length=50)

    def __str__(self):
        return self.list
